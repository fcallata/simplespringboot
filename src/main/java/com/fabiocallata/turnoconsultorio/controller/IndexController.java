package com.fabiocallata.turnoconsultorio.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

	private static Logger LOG = LogManager.getLogger(IndexController.class);
	
	@GetMapping("/index")
	public ModelAndView principal() {
		LOG.info("Index principal");
		Map<String, Object> mapJson = new HashMap<>();
		
		String mensaje = "Hola fabio";
		mapJson.put("saludo", "Hola Fabio Callata!");
		return new ModelAndView("index").addAllObjects(mapJson).addObject("mensaje", mensaje);
	}
}
