package com.fabiocallata.turnoconsultorio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class TurnoconsultorioApplication {

	public static void main(String[] args) {
		SpringApplication.run(TurnoconsultorioApplication.class, args);
	}
}
